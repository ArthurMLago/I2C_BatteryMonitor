




V1 alim 0 DC 5V

VSGND sgnd 0 DC 300V

VS0 s0 sgnd DC 2 sin(2 1 100 0 0)
VS1 s1 s0 DC 2 sin(2 1 80 0 0)
VS2 s2 s1 DC 2 sin(2 1 70 0 0)
VS3 s3 s2 DC 2 sin(2 1 110 0 0)
VS4 s4 s3 DC 2 sin(2 1 200 0 0)
VS5 s5 s4 DC 2 sin(2 1 8 0 0)


X0 pIn0 mIn0 alim 0 s0_read LM324
R0_0 s0 pIn0 1Meg
R0_1 pIn0 0 1Meg
R0_2 sgnd mIn0 1Meg
R0_3 mIn0 s0_read 1Meg

X1 pIn1 mIn1 alim 0 s1_read LM324
R1_0 s1 pIn1 1Meg
R1_1 pIn1 0 1Meg
R1_2 s0 mIn1 1Meg
R1_3 mIn1 s1_read 1Meg

X2 pIn2 mIn2 alim 0 s2_read LM324
R2_0 s2 pIn2 1Meg
R2_1 pIn2 0 1Meg
R2_2 s1 mIn2 1Meg
R2_3 mIn2 s2_read 1Meg

X3 pIn3 mIn3 alim 0 s3_read LM324
R3_0 s3 pIn3 1Meg
R3_1 pIn3 0 1Meg
R3_2 s2 mIn3 1Meg
R3_3 mIn3 s3_read 1Meg

X4 pIn4 mIn4 alim 0 s4_read LM324
R4_0 s4 pIn4 1Meg
R4_1 pIn4 0 1Meg
R4_2 s3 mIn4 1Meg
R4_3 mIn4 s4_read 1Meg

X5 pIn5 mIn5 alim 0 s5_read LM324
R5_0 s5 pIn5 1Meg
R5_1 pIn5 0 1Meg
R5_2 s4 mIn5 1Meg
R5_3 mIn5 s5_read 1Meg


* X1 LM324 pIn1 mIn1 alim 0 s1_read
* X2 LM324 pIn2 mIn2 alim 0 s2_read
* X3 LM324 pIn3 mIn3 alim 0 s3_read
* X4 LM324 pIn4 mIn4 alim 0 s4_read
* X5 LM324 pIn5 mIn5 alim 0 s5_read










* LM324 OPERATIONAL AMPLIFIER "MACROMODEL" SUBCIRCUIT
* CREATED USING PARTS RELEASE 4.01 ON 09/08/89 AT 10:54
* (REV N/A)      SUPPLY VOLTAGE: 5V
* CONNECTIONS:   NON-INVERTING INPUT
*                | INVERTING INPUT
*                | | POSITIVE POWER SUPPLY
*                | | | NEGATIVE POWER SUPPLY
*                | | | | OUTPUT
*                | | | | |
.SUBCKT LM324    1 2 3 4 5
*
  C1   11 12 5.544E-12
  C2    6  7 20.00E-12
  DC    5 53 DX
  DE   54  5 DX
  DLP  90 91 DX
  DLN  92 90 DX
  DP    4  3 DX
  EGND 99  0 POLY(2) (3,0) (4,0) 0 .5 .5
  FB    7 99 POLY(5) VB VC VE VLP VLN 0 15.91E6 -20E6 20E6 20E6 -20E6
  GA    6  0 11 12 125.7E-6
  GCM   0  6 10 99 7.067E-9
  IEE   3 10 DC 10.04E-6
  HLIM 90  0 VLIM 1K
  Q1   11  2 13 QX
  Q2   12  1 14 QX
  R2    6  9 100.0E3
  RC1   4 11 7.957E3
  RC2   4 12 7.957E3
  RE1  13 10 2.773E3
  RE2  14 10 2.773E3
  REE  10 99 19.92E6
  RO1   8  5 50
  RO2   7 99 50
  RP    3  4 30.31E3
  VB    9  0 DC 0
  VC 3 53 DC 2.100
  VE   54  4 DC .6
  VLIM  7  8 DC 0
  VLP  91  0 DC 40
  VLN   0 92 DC 40
.MODEL DX D(IS=800.0E-18)
.MODEL QX PNP(IS=800.0E-18 BF=250)
.ENDS






.control
* dc Ilight 0 0.5 10u

destroy all

TRAN 10u 100m
plot v(s0,sgnd) v(s0_read)
plot v(s1,s0) v(s1_read)
plot v(s2,s1) v(s2_read)
plot v(s3,s2) v(s3_read)
plot v(s4,s3) v(s4_read)
plot v(s5,s4) v(s5_read)


